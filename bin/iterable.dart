import 'package:iterable/iterable.dart' as iterable;

void main(List<String> arguments) {
  Iterable<int> iterable = [1, 2, 3];
  int value = iterable.elementAt(1);
  print(value);

  //Set, List -> Iterable
  const foods = ['Salad', 'Popcorn', 'Toast', 'Lasagne', 'Mamama'];
  Iterable<String> iterableFoods = foods;
  for (var food in iterableFoods) {
    print(food);
  }

  print('First element is ${foods.first}');
  print('Last element is ${iterableFoods.last}');

  var foundItem1 = foods.firstWhere((element) {
    return element.length > 5;
  });
  print(foundItem1);

  var foundItem2 = foods.firstWhere((element) => element.length > 5);
  print(foundItem2);

  bool predicate(String item) {
    return item.length > 5;
  }

  var foundItem3 = foods.firstWhere(predicate);
  print(foundItem3);

  var foundItem4 =
      foods.firstWhere((item) => item.length > 10, orElse: () => 'None!');
  print(foundItem4);

  var foundItem5 = foods.firstWhere((item) => item.startsWith('M') && item.contains('a'),orElse: () => 'None!');
  print(foundItem5);

  if(foods.any((item) => item.contains('a'))){
    print('At least on item contains "a"');
  }

  if (foods.every((item) => item.length >= 5)) {
    print('All item have lenght >= 5');
  }
}

